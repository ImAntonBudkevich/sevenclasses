class User {
  constructor() {
    this._loginStatus = '';
    this._registerDate = Date.now();
  }

  set userId(id) {
    if (typeof id !== 'string' || id === '') {
      throw new Error('not a name');
    }
    this._userId = id;
  }

  get userId() {
    return this._userId;
  }

  set password(pass) {
    if (pass === '') {
      throw new Error('enter password');
    }
    this._password = pass;
  }

  verifyLogin() {
    console.log('loginpass');
  }
}

class Administrator extends User {
  constructor() {
    super();
    this._adminName = '';
  }

  set email(mail) {
    if (mail.indexof('@') === -1) {
      //do not work??
      throw new Error('not a mail');
    }
    this._email = mail;
  }

  get email() {
    return this._email;
  }

  updateCatalog() {
    return true;
  }
}

class Customer extends User {
  constructor() {
    super();
    this._customerName = '';
    this._address = '';
    this._email = '';
    this._shippingInfo = '';
    this._accountbalance = parseFloat();
  }

  set creditCardInfo(number) {
    if (number.toString().length !== 16) {
      throw new Error('not a credic card serial');
    }
    this._creditCardInfo = number;
  } //ne sozdaet perem

  get creditCardInfo() {
    return this._creditCardInfo;
  }

  register() {
    console.log('registered');
  }
  login() {
    console.log('logined');
  }
  updateProfile() {
    console.log('updated');
  }
}

let user1 = new Customer();
try {
  user1.userId = '34';
} catch (e) {
  console.log(e.message);
}

try {
  user1.password = 'dgfhgfh';
} catch (e) {
  console.log(e.message);
}

try {
  user1.email = 'fdgdkjsfgdfk@lgs.com';
} catch (e) {
  console.log(e.message);
}

try {
  user1.creditCardInfo = '2387968574895567';
} catch (e) {
  console.log(e.message);
}

console.log(user1);
// klassi v klassah?

class ShoppingCart {
  constructor() {
    this._cartId = new BigInt();
    this._productId = new BigInt();
    this._quantity = new BigInt();
    this._dateAdded = new BigInt();
  }

  addCartItem() {
    console.log('added');
  }
  updateQuantity() {
    console.log('updated');
  }
  viewCartDetails() {
    console.log('showed');
  }
  checkOut() {
    console.log('checked');
  }
}

class Order {
  constructor() {
    this._orderId = '';
    this._dateCreated;
    this._dateShipped;
    this._customerName;
    this._customerId;
    this._status;
    this._shippingId;
  }
  placeOrder() {
    console.log('placed');
  }
}
class ShippingInfo {
  constructor() {
    this._shippingId;
    this._shippingType;
    this._shippingCost;
    this._shippingRegionId;
  }
  updateShippingInfo() {
    console.log('info');
  }
}
class OrderDetails {
  constructor() {
    this._orderId;
    this._productId;
    this._productName;
    this._quantity;
    this._unitCost;
    this._subtotal;
  }

  calcPrice() {
    console.log('summ');
  }
}
